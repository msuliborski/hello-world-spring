FROM openjdk:13
ADD target/hello-world-spring.jar hello-world-spring.jar
ENTRYPOINT ["java","-jar","/hello-world-spring.jar"]